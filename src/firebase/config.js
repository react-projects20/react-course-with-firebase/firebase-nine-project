import { initializeApp } from 'firebase/app'
import {getFirestore} from 'firebase/firestore'
import {getAuth} from 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyDZ31awlq54YPtOLhq7p2J2-7tCEwBKG4I",
    authDomain: "readinglistapp-e937e.firebaseapp.com",
    projectId: "readinglistapp-e937e",
    storageBucket: "readinglistapp-e937e.appspot.com",
    messagingSenderId: "119235381304",
    appId: "1:119235381304:web:3e93fb724c7f9867a73ecd"
};

//init firebase
initializeApp(firebaseConfig)

//init firestore
const db = getFirestore()

//init firebase auth
const auth = getAuth()

export {db, auth}